import datetime
from typing import Optional, List

from fastapi import FastAPI, Query
from starlette.middleware.cors import CORSMiddleware

from tncmvp.common import config
from tncmvp.parser.queue import twitter_accounts

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        'http://localhost:8080'
    ],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.on_event('startup')
async def setup():
    await config.setup_connections()


@app.get('/events')
async def get_events(
        since: Optional[datetime.datetime] = None,
        till: Optional[datetime.datetime] = None,
        accounts: Optional[List[str]] = Query(None),
):
    search_terms = {'reply_to': []}
    if since:
        search_terms['timestamp'] = search_terms.get('timestamp', {}) | {'$gte': since}
    if till:
        search_terms['timestamp'] = search_terms.get('timestamp', {}) | {'$lte': till}
    if accounts:
        search_terms['account'] = {'$in': accounts}
    twitter_events = config.mongo.twitter_events.find(search_terms).sort('timestamp', 1)
    events = [
        {
            'timestamp': event['timestamp'].timestamp(),
            'source': 'twitter',
            'image': event['raw_data']['thumbnail'] if 'thumbnail' in event['raw_data'] else None,
        } | {
            k: v for k, v in event.items() if k in ['account', 'text', 'tags', 'reply_to']
        }
        async for event in twitter_events
    ]
    return events


@app.get('/sources')
def get_sources():
    return list(twitter_accounts())
