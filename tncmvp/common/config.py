import datetime
import logging
from typing import Optional

import motor.motor_asyncio
from pydantic import BaseSettings, AnyUrl


class Settings(BaseSettings):
    mongo_url: AnyUrl = 'mongodb://localhost:27017'
    parse_since: datetime.datetime = datetime.datetime(year=2020, month=1, day=1)


settings = Settings()
motor_client: Optional[motor.motor_asyncio.AsyncIOMotorClient] = None
mongo: Optional[motor.motor_asyncio.AsyncIOMotorDatabase] = None


async def create_indexes():
    await mongo.twitter_events.create_index('timestamp')
    logging.debug('indexes created')


async def setup_connections():
    global motor_client, mongo
    motor_client = motor.motor_asyncio.AsyncIOMotorClient(settings.mongo_url)
    mongo = motor_client.tncmvp
    logging.debug('db initialized')
    await create_indexes()
