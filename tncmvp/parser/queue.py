import os

from tncmvp.parser.twitter import TwitterParser


def twitter_accounts():
    with open(os.path.join(os.path.dirname(__file__), '..', 'assets', 'twitter_pages.txt'), 'r', encoding='utf-8') as f:
        for account in f:
            account = account.strip()
            if not account.startswith('#'):
                yield account

async def parse_twitter():
    for account in twitter_accounts():
        parser = TwitterParser(account)
        await parser.run()

async def main():
    await parse_twitter()
