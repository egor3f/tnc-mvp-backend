import datetime
import logging
from typing import List, Optional

import twint
from pymongo import UpdateOne
from twint import output
from twint.run import Twint

from tncmvp.common import config
from tncmvp.common.config import settings


class TwitterParser:
    def __init__(self, account: str):
        self.account = account

    async def run(self):
        page = await config.mongo.twitter_pages.find_one({'account': self.account})
        start, end = self.get_parsing_interval(page['parsed_since'] if page else None, page['parsed_till'] if page else None)
        current_start = start
        while current_start <= end:
            current_end = current_start + datetime.timedelta(weeks=1)
            logging.debug(f'Parsing twitter {self.account} from {current_start} to {current_end}')
            tweets = await self.parse_chunk(current_start, current_end)

            requests = [
                UpdateOne({
                    'account': self.account,
                    'timestamp': datetime.datetime.strptime(tweet.datetime, '%Y-%m-%d %H:%M:%S %Z'),
                }, {'$set': {
                    'text': tweet.tweet,
                    'tags': tweet.hashtags,
                    'reply_to': tweet.reply_to,
                    'raw_data': tweet.__dict__,
                }}, True)
                for tweet in tweets
            ]
            if requests:
                await config.mongo.twitter_events.bulk_write(requests)

            save_end = min(current_end, datetime.datetime.now())
            await config.mongo.twitter_pages.update_one({
                'account': self.account
            }, {'$set': {
                'parsed_since': min(start, page['parsed_since']) if page else start,
                'parsed_till': max(save_end, page['parsed_till']) if page else save_end,
            }}, True)

            current_start = current_end
            # await asyncio.sleep(1)

    async def parse_chunk(self, date_start: datetime.datetime, date_end: datetime.datetime) -> List:
        c = twint.Config()
        c.Username = self.account
        c.Since = date_start.strftime('%Y-%m-%d')
        c.Until = date_end.strftime('%Y-%m-%d')
        c.TwitterSearch = True
        c.Store_object = True
        await Twint(c).run()
        result = output.tweets_list.copy()
        output.tweets_list[:] = []
        return result

    def get_parsing_interval(self, parsed_since: Optional[datetime.datetime], parsed_till: Optional[datetime.datetime]):
        start = settings.parse_since
        end = datetime.datetime.now()  # .replace(hour=0, minute=0, second=0)  # today
        if parsed_till and (not parsed_since or parsed_since <= settings.parse_since):
            start = parsed_till  # + datetime.timedelta(days=1)
        if parsed_since and parsed_since > settings.parse_since:
            end = settings.parse_since
        return start, end
