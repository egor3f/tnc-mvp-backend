import asyncio

from tncmvp.common import config
from tncmvp.parser import queue


async def main():
    await config.setup_connections()
    await queue.main()

if __name__ == '__main__':
    asyncio.run(main())
